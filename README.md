# Exercice 3 : CICD

## Michel Ngor Mack NDONG et EL Hadji Amadou CISS

# Consigne :  binôme : 
- Créer un dépôt privé GitLab et le partager avec le compte devopssenegal. 
- Attribuer le rôle "maintainer" au compte devopssenegal
- Permettre la création automatique d'image docker pour l'application web 
- Stocker les images dans GitLab Container Registry de votre dépot
- Créer un fichier docker compose utilisant la dernière version de votre image (celle se trouvant dans votre registry gitlab)
- Déployer l'application sur Heroku : 
    - Permettre le déploiement continu de l'application
    - Me partager le lien de l'application


https://projet-ndong-ciss.herokuapp.com/
